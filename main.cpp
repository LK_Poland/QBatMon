#include "mainwindow.h"
#include <QApplication>
#include <QDesktopWidget>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setQuitOnLastWindowClosed(false);

    const QWidget *screen = QDesktopWidget().screen();
    const QSize size = screen->size();

    MainWindow w;
    w.move(size.width() / 2 - w.width() / 2,
           size.height() / 2 - w.height() / 2);
    //w.show();

    return a.exec();
}
