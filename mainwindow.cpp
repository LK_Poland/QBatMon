#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "about.h"

#include <QDebug>
#include <QMessageBox>
#include <QProcess>

static const QString appVer = "1.3";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(&tmrBatInfo, SIGNAL(timeout()), this, SLOT(updateBatInfo()));

    QMenu *menu = new QMenu(this);
    menu->addAction("About...", this, SLOT(aboutApp()));
    menu->addAction("About Qt...", this, SLOT(aboutQt()));
    menu->addSeparator();
    menu->addAction("Close", this, SLOT(leave()));

    trayIcon.setIcon(QIcon(":/battery-missing"));
    trayIcon.setContextMenu(menu);
    trayIcon.show();

    tmrBatInfo.start(3000);
}

MainWindow::~MainWindow()
{
    delete ui;
}

QString MainWindow::getIconString(const int percent, const bool charging)
{
    static bool lowBatteryShown = false;
    static bool flatBatteryShown = false;

    QString icon = ":/battery-";

    if (charging)
    {
        icon += "charging-";
    }

    if (percent > 80)
    {
        icon += "100";
    }
    else if (percent > 60)
    {
        icon += "80";
    }
    else if (percent > 40)
    {
        icon += "60";
    }
    else if (percent > 20)
    {
        lowBatteryShown = false;
        icon += "40";
    }
    else if (percent > 10)
    {
        if ((false == lowBatteryShown) &&
            (false == charging))
        {
            lowBatteryShown = true;
            trayIcon.showMessage("Low battery",
                                 "Battery is low, plug power cord.",
                                 QSystemTrayIcon::Warning);
        }
        flatBatteryShown = false;
        icon += "caution";
    }
    else
    {
        if ((false == flatBatteryShown) &&
            (false == charging))
        {
            flatBatteryShown = true;
            trayIcon.showMessage("Flat battery",
                                 "Battery is flat, plug power cord immediately.",
                                 QSystemTrayIcon::Critical);
        }
        icon += "low";
    }

    return icon;
}

void MainWindow::updateBatInfo()
{
    QStringList args;
    args << "-b" << "-p";

    QProcess acpi;
    acpi.start("acpi", args);
    acpi.waitForFinished();

    QString info(acpi.readAllStandardOutput());
    //qDebug() << info;

    const int start = info.indexOf(':') + 2;
    if (1 == start)
    {
        return;
    }

    const QString core = info.mid(start).remove('\n');
    const QStringList list = core.split(", ");
//list.removeLast();
    if (list.size() < 2)
    {
        return;
    }
    /*
    for (auto item : list)
    {
        qDebug() << item;
    }
*/
    const int percent = QString(list[1]).remove("%").toInt();

    QString tipText = list[0] + ", " + list[1];
    if (list.size() > 2)
    {
        tipText += ", " + list[2];
    }

    trayIcon.setToolTip(tipText);
    trayIcon.setIcon(QIcon(getIconString(percent, "charging" == list[0])));
}

void MainWindow::aboutApp()
{
    About about(this);
    about.setAppVer(appVer);
    about.exec();
}

void MainWindow::aboutQt()
{
    QMessageBox::aboutQt(this);
}

void MainWindow::leave()
{
    qApp->quit();
}
