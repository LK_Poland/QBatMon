#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSystemTrayIcon>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    QString getIconString(const int percent, const bool charging);

protected slots:
    void updateBatInfo();
    void aboutApp();
    void aboutQt();
    void leave();

private:
    Ui::MainWindow *ui;
    QTimer tmrBatInfo;
    QSystemTrayIcon trayIcon;
};

#endif // MAINWINDOW_H
